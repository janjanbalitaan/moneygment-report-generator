#!/bin/bash
cd /home/user/moneygment-report-generator/
source venv/bin/activate
source environments/sample-daily-report.env
python scripts/csv-report.py
